package ru.inshakov.tm.api.service;

import ru.inshakov.tm.model.Task;
import ru.inshakov.tm.model.Project;

import java.util.List;

public interface IProjectTaskService {

    List<Task> findAllTasksByProjectId(String projectId);

    Task bindTaskByProject(String projectId, String taskId);

    Task unbindTaskFromProject(String projectId, String taskId);

    Project removeProjectById(String projectId);

}
