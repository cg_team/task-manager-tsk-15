package ru.inshakov.tm.controller;

import ru.inshakov.tm.api.controller.IProjectTaskController;
import ru.inshakov.tm.api.service.IProjectTaskService;
import ru.inshakov.tm.exception.entity.ProjectNotFoundException;
import ru.inshakov.tm.exception.entity.TaskNotFoundException;
import ru.inshakov.tm.model.Project;
import ru.inshakov.tm.model.Task;
import ru.inshakov.tm.util.TerminalUtil;

import java.util.List;

public class ProjectTaskController implements IProjectTaskController {

    private final IProjectTaskService projectTaskService;

    public ProjectTaskController(IProjectTaskService projectTaskService) {
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void showAllTasksByProjectId() {
        System.out.println("[ENTER PROJECT ID:]");
        final String projectId = TerminalUtil.nextLine();
        List<Task> tasksByProjectId = projectTaskService.findAllTasksByProjectId(projectId);
        for (final Task task: tasksByProjectId) {
            System.out.println(task);
        }
    }

    @Override
    public void bindTaskByProject() {
        System.out.println("[ENTER PROJECT ID:]");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("[ENTER TASK ID:]");
        final String taskId = TerminalUtil.nextLine();
        projectTaskService.bindTaskByProject(projectId, taskId);
    }

    @Override
    public void unbindTaskFromProject() {
        System.out.println("[ENTER PROJECT ID:]");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("[ENTER TASK ID:]");
        final String taskId = TerminalUtil.nextLine();
        final Task task = projectTaskService.unbindTaskFromProject(projectId, taskId);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void removeProjectById() {
        System.out.println("[ENTER PROJECT ID:]");
        final String projectId = TerminalUtil.nextLine();
        final Project project = projectTaskService.removeProjectById(projectId);
        if (project == null) throw new ProjectNotFoundException();
    }

}
