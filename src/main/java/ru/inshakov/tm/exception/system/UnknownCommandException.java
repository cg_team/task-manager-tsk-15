package ru.inshakov.tm.exception.system;

public class UnknownCommandException extends RuntimeException {

    public UnknownCommandException() {
        super("Error! Command not found...");
    }

}
