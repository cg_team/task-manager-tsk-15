package ru.inshakov.tm.exception.system;

public class UnknownArgumentException extends RuntimeException {

    public UnknownArgumentException() {
        super("Error! Argument not found...");
    }

}
